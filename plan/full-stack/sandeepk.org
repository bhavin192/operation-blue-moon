#+TITLE: Plan
#+AUTHOR: Sandeep Kumar Choudhary
#+EMAIL: sandeepchoudhary1507@gmail.com
#+TAGS: read write dev ops task event meeting # Need to be category
* GOALS
** Books to read [30%]
   :PROPERTIES:
    :ESTIMATED: 
    :ACTUAL:
    :OWNER: sandeepk
    :ID: READ.1553504274
    :TASKID: READ.1553504274
    :END:
   - [X] Sapiens A Breif History of HumanKind.
   - [X] Start with Why: How great Leaders Inspires Everyone To Take Action
   - [ ] The Personal MBA:A World-Class Busniess Education in a Single Volume
   - [X] Atomic Habits
   - [ ] Operating System
   - [ ] Art of Computer Programming, The: Volume 1
   - [ ] Art of Computer Programming, The: Volume 2
   - [ ] Art of Computer Programming, The: Volume 3
   - [ ] Art of Computer Programming, The: Volume 4
   - [ ] The Majesty of Vue.js
   - [ ] Clean Code
   - [ ] 97 Things Every Engineering Manager Should Know
   - [X] Software Engg Essays
   - [ ] Math for programmers
     
** Database
*** MongoDB
    :PROPERTIES:
    :ESTIMATED: 
    :ACTUAL:
    :OWNER: sandeepk
    :ID: READ.1553504661
    :TASKID: READ.1553504661
    :END:
** FrameWork
*** [#A] Django
    :PROPERTIES:
    :ESTIMATED: 
    :ACTUAL:
    :OWNER: sandeepk
    :ID: READ.1553504708
    :TASKID: READ.1553504708
    :END:
    - [ ] Setup django project from scratch for revision
    - [ ] Under stand Prefetch and SelectReleated use case
*** Vue.js
    :PROPERTIES:
    :ESTIMATED: 
    :ACTUAL:
    :OWNER: sandeepk
    :ID: READ.1553504723
    :TASKID: READ.1553504723
    :END:
*** Node
    :PROPERTIES:
    :ESTIMATED: 
    :ACTUAL:
    :OWNER: sandeepk
    :ID: READ.1553504753
    :TASKID: READ.1553504753
    :END:
** Projects
*** DONE [#A] Stacknews [/]
    :PROPERTIES:
    :ESTIMATED: 
    :ACTUAL:
    :OWNER: sandeepk
    :ID: DEV.1553504808
    :TASKID: DEV.1553504808
    :END:
*** [#B] Wagtail [%]
   :PROPERTIES:
   :ESTIMATED: 13
   :ACTUAL:
   :OWNER: sandeepk
   :ID: DEV.1598776989
   :TASKID: DEV.1598776989
   :END:
   - [ ] Read the guidelines                                     ( 60m  )
   - [ ] Explore around the project, find the issue to work on   ( 300m )
   - [ ] Work on some patches                                    ( 420m )   
*** 2Pie
   :PROPERTIES:
   :ESTIMATED: 
   :ACTUAL:
   :OWNER: sandeepk
   :ID: DEV.1598777080
   :TASKID: DEV.1598777080
   :END:
   - [ ] Enable the async function calling to handle slack timeout.
   - [ ] List down all the requirements
   - [ ] Write down the setup guide
   - [ ] API to fetch all the commits from github Repo
   - [ ] Method to compose the Stand up notes based on commits fetched
*** Android App
   :PROPERTIES:
   :ESTIMATED: 
   :ACTUAL:
   :OWNER: sandeepk
   :ID: DEV.1598777266
   :TASKID: DEV.1598777266
   :END:
   - [ ] Layout the design for the app flow.
   - [ ] Create the multiple screen for the user flow.
*** Tally Data Fetcher
   :PROPERTIES:
   :ESTIMATED: 
   :ACTUAL:
   :OWNER: sandeepk
   :ID: DEV.1598777376
   :TASKID: DEV.1598777376
   :END:
   - [ ] Read the docs, how to interact with the tally server.

** Courses
*** DONE [#A] Google Cloud Specialization
    :PROPERTIES:
    :ESTIMATED: 58
    :ACTUAL:
    :OWNER: sandeepk
    :ID: READ.1559492157
    :TASKID: READ.1559492157
    :END:
*** DONE Practical Python Programming
   :PROPERTIES:
   :ESTIMATED: 16
   :ACTUAL:
   :OWNER: sandeepk
   :ID: READ.1598776776
   :TASKID: READ.1598776776
   :END:
   - [X] Chapter 4 Classes
   - [X] Chapter 5 Inner Working
   - [X] Chapter 6 Generatore
   - [X] Chapter 7 Advance Topics
   - [X] Chapter 8 Testing
   - [X] Chapter 9 Packages

*** Math for programmers [%]
    :PROPERTIES:...
    - [ ] Chapter 1 - Prime Numbers         ( 2hr )
    - [ ] Chapter 2 - Modulo arithmetic     ( 3hr )
    - [ ] Chapter 3 - Probability           ( 4hr )
    - [ ] Chapter 4 - Combinatorics         ( 4hr )

* PLAN
** November   16, 2020 - November  28, 2020 (13 days)
   :PROPERTIES:
   :wpd-sandeepk: 1
   :END:

** November   29, 2020 - December  15, 2020 (17 days)
   :PROPERTIES:
   :wpd-sandeepk: 1
   :END:

** December   16, 2020 - December  28, 2020 (13 days)
   :PROPERTIES:
   :wpd-sandeepk: 1
   :END:

   


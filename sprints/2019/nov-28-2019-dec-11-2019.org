
#+TITLE: November 28, 2019 - December 11, 2019 (14 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 14
  :SPRINTSTART: <2019-11-28 Thu>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-kurianbenoy: 1.5
  :wpd-sandeepk: 1.2
  :END:
** akshay196
*** DONE Test-Driven Development with Python - Part II [16/16]
    CLOSED: [2019-12-11 Wed 22:25]
    :PROPERTIES:
    :ESTIMATED: 14
    :ACTUAL:   14.63
    :OWNER: akshay196
    :ID: READ.1573574122
    :TASKID: READ.1573574122
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-11 Wed 21:15]--[2019-12-11 Wed 22:25] =>  1:10
    CLOCK: [2019-12-11 Wed 16:01]--[2019-12-11 Wed 16:55] =>  0:54
    CLOCK: [2019-12-11 Wed 08:50]--[2019-12-11 Wed 09:18] =>  0:28
    CLOCK: [2019-12-10 Tue 18:05]--[2019-12-10 Tue 19:55] =>  1:50
    CLOCK: [2019-12-10 Tue 09:00]--[2019-12-10 Tue 10:03] =>  1:03
    CLOCK: [2019-12-09 Mon 19:04]--[2019-12-09 Mon 20:03] =>  0:59
    CLOCK: [2019-12-09 Mon 08:58]--[2019-12-09 Mon 10:03] =>  1:05
    CLOCK: [2019-12-08 Sun 08:56]--[2019-12-08 Sun 09:58] =>  1:02
    CLOCK: [2019-12-07 Sat 08:20]--[2019-12-07 Sat 08:55] =>  0:35
    CLOCK: [2019-12-05 Thu 08:39]--[2019-12-05 Thu 09:23] =>  0:44
    CLOCK: [2019-12-04 Wed 20:40]--[2019-12-04 Wed 21:41] =>  1:01
    CLOCK: [2019-12-03 Tue 09:02]--[2019-12-03 Tue 09:48] =>  0:46
    CLOCK: [2019-12-02 Mon 08:39]--[2019-12-02 Mon 09:45] =>  1:06
    CLOCK: [2019-11-30 Sat 09:43]--[2019-11-30 Sat 11:02] =>  1:19
    CLOCK: [2019-11-29 Fri 08:22]--[2019-11-29 Fri 08:58] =>  0:36
    :END:
    - [X] Chapter 11. Automating Deployment with Fabric                                             (30m)
    - [X] Chapter 12. Splitting Our Tests into Multiple Files, and a Generic Wait Helper            (30m)
    - [X] Chapter 13. Validation at the Database Layer                                              ( 1h)
    - [X] Chapter 14. A Simple Form                                                                 ( 1h)
    - [X] Chapter 15. More Advanced Forms                                                           ( 1h)
    - [X] Chapter 16. Dipping Our Toes, Very Tentatively, into JavaScript                           ( 1h)
    - [X] Chapter 17. Deploying Our New Code                                                        (30m)
    - [X] Chapter 18. User Authentication, Spiking, and De-Spiking                                  ( 1h)
    - [X] Chapter 19. Using Mocks to Test External Dependencies or Reduce Duplication               ( 1h)
    - [X] Chapter 20. Test Fixtures and a Decorator for Explicit Waits                              (30m)
    - [X] Chapter 21. Server-Side Debugging                                                         ( 1h)
    - [X] Chapter 22. Finishing "My Lists": Outside-In TDD                                          ( 1h)
    - [X] Chapter 23. Test Isolation, and "Listening to Your Tests"                                 ( 1h)
    - [X] Chapter 24. Continuous Integration (CI)                                                   ( 1h)
    - [X] Chapter 25. The Token Social Bit, the Page Pattern, and an Exercise for the Reader        ( 1h)
    - [X] Chapter 26. Fast Tests, Slow Tests, and Hot Lava                                          ( 1h)
** bhavin192
*** DONE Certified Kubernetes Application Developer exam [2/2]
    CLOSED: [2019-11-30 Sat 14:02]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.02
    :OWNER:    bhavin192
    :ID:       OPS.1575116208
    :TASKID:   OPS.1575116208
    :END:
    :LOGBOOK:
    CLOCK: [2019-11-30 Sat 11:34]--[2019-11-30 Sat 14:02] =>  2:28
    CLOCK: [2019-11-29 Fri 22:35]--[2019-11-29 Fri 23:01] =>  0:26
    CLOCK: [2019-11-29 Fri 18:55]--[2019-11-29 Fri 20:02] =>  1:07
    :END:
     https://www.cncf.io/certification/ckad/
     - [X] Practice	 (2h)
     - [X] Exam		 (2h)
*** DONE Watch EmacsConf 2019 videos [14/14]
    CLOSED: [2019-12-10 Tue 19:57]
    :PROPERTIES:
    :ESTIMATED: 4.5
    :ACTUAL:   5.17
    :OWNER:    bhavin192
    :ID:       READ.1575285614
    :TASKID:   READ.1575285614
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-10 Tue 19:36]--[2019-12-10 Tue 19:57] =>  0:21
    CLOCK: [2019-12-10 Tue 17:19]--[2019-12-10 Tue 18:01] =>  0:42
    CLOCK: [2019-12-09 Mon 19:02]--[2019-12-09 Mon 20:24] =>  1:22
    CLOCK: [2019-12-08 Sun 22:30]--[2019-12-08 Sun 22:52] =>  0:22
    CLOCK: [2019-12-08 Sun 20:03]--[2019-12-08 Sun 20:30] =>  0:27
    CLOCK: [2019-12-05 Thu 18:57]--[2019-12-05 Thu 20:18] =>  1:21
    CLOCK: [2019-12-03 Tue 20:51]--[2019-12-03 Tue 20:57] =>  0:06
    CLOCK: [2019-12-03 Tue 07:51]--[2019-12-03 Tue 08:08] =>  0:17
    CLOCK: [2019-12-02 Mon 21:51]--[2019-12-02 Mon 21:58] =>  0:07
    CLOCK: [2019-12-02 Mon 19:35]--[2019-12-02 Mon 19:40] =>  0:05
    :END:
    - [X] [[https://media.emacsconf.org/2019/02.html][Emacs Community Update - Sacha Chua]]                            (10m)
    - [X] [[https://media.emacsconf.org/2019/12.html][Ledger-mode - Quiliro Ordóñez]]                                  (15m)
    - [X] [[https://media.emacsconf.org/2019/14.html][Magit deep dive - Jonathan Chu]]                                 (50m)
    - [X] [[https://media.emacsconf.org/2019/19.html][How Emacs became my awesome Java editing environment -
      Torstein Krause Johansen]]                                           (15m)
    - [X] [[https://media.emacsconf.org/2019/20.html][Automate your workflow as a game developer - Jānis Mancēvičs]]   (10m)
    - [X] [[https://media.emacsconf.org/2019/22.html][Packaging emacs packages for Debian - David Bremner]]            (10m)
    - [X] [[https://media.emacsconf.org/2019/24.html][GNU Emacs as software freedom in practice - Greg Farough]]       (25m)
    - [X] [[https://media.emacsconf.org/2019/26.html][Emacs: The Editor for the Next Forty Years - Perry
      E. Metzger]]                                                         (65m)
    - [X] [[https://media.emacsconf.org/2019/27.html][How to record executable notes with eev - and how to play
      them back - Eduardo Ochs]]                                           (20m)
    - [X] [[https://media.emacsconf.org/2019/28.html][Play and control your music with Emacs - Damien Cassou]]         (10m)
    - [X] [[https://media.emacsconf.org/2019/29.html][Restclient and org-mode for Api Documentation and Testing -
      Mackenzie Bligh]]                                                    (10m)
    - [X] [[https://media.emacsconf.org/2019/30.html][Equake mode - Ben Slade]]                                        (10m)
    - [X] [[https://media.emacsconf.org/2019/31.html][Don't wait! Write your own (yas)snippet - Tony Aldon]]           (10m)
    - [X] [[https://media.emacsconf.org/2019/32.html][VSCode is Better than Emacs - Zaiste]]                           (10m)
** kurianbenoy
*** DONE  Deep Learning with Pytorch [4/4]
    :PROPERTIES:
    :ESTIMATED: 12
    :ACTUAL:   8.86
    :OWNER: kurianbenoy
    :ID: READ.1575047741
    :TASKID: READ.1575047741
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-12 Thu 18:47]--[2019-12-12 Thu 19:27] =>  0:40
    CLOCK: [2019-12-12 Thu 15:24]--[2019-12-12 Thu 16:32] =>  1:08
    CLOCK: [2019-12-10 Tue 14:19]--[2019-12-10 Tue 15:09] =>  0:50
    CLOCK: [2019-12-09 Mon 20:41]--[2019-12-09 Mon 20:52] =>  0:11
    CLOCK: [2019-12-09 Mon 17:52]--[2019-12-09 Mon 18:19] =>  0:27
    CLOCK: [2019-12-09 Mon 09:12]--[2019-12-09 Mon 09:48] =>  0:36
    CLOCK: [2019-12-07 Sat 17:58]--[2019-12-07 Sat 18:38] =>  0:40
    CLOCK: [2019-12-06 Fri 14:15]--[2019-12-06 Fri 15:05] =>  0:50
    CLOCK: [2019-11-30 Sat 08:00]--[2019-11-30 Sat 09:30] =>  1:30
    CLOCK: [2019-12-01 Sun 12:00]--[2019-12-01 Sun 13:00] =>  1:00
    CLOCK: [2019-12-06 Fri 08:55]--[2019-12-06 Fri 09:55] =>  1:00
    :END:
    - [X] Chapter2: It starts with a Tensor                 (2h)
    - [X] Chapter 3: Real World representation with Tensors (3h)
    - [X] Chapter 4: Mechanism of Learning                  (5h)
    - [X] Chapter 5: Using a NN to fit data                 (2h)
*** DONE READ CTCI - Part I  [1/1]
    CLOSED: [2019-12-11 Wed 23:00]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   5.83
    :OWNER: kurianbenoy
    :ID: READ.1575048222
    :TASKID: READ.1575048222
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-05 Thu 20:45]--[2019-12-05 Thu 21:15] =>  0:30
    CLOCK: [2019-12-05 Thu 13:26]--[2019-12-05 Thu 14:00] =>  0:34
    CLOCK: [2019-12-05 Thu 08:24]--[2019-12-05 Thu 09:25] =>  1:01
    CLOCK: [2019-12-04 Wed 16:00]--[2019-12-04 Wed 16:45] =>  0:45
    CLOCK: [2019-12-03 Tue 22:00]--[2019-12-03 Tue 23:00] =>  1:00
    CLOCK: [2019-12-02 Mon 16:00]--[2019-12-02 Mon 18:00] =>  2:00
    :END:
    - [X] Trees and graphs                  (4h)
*** DONE Onboard GCI participants | Mentor work Part II
    CLOSED: [2019-12-11 Wed 23:00]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   2.33
    :OWNER: kurianbenoy
    :ID: PROJECT.1575302444
    :TASKID: PROJECT.1575302444
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-07 Sat 13:18]--[2019-12-07 Sat 13:41] =>  0:23
    CLOCK: [2019-12-07 Sat 07:42]--[2019-12-07 Sat 08:44] =>  1:02
    CLOCK: [2019-12-06 Fri 06:59]--[2019-12-06 Fri 07:54] =>  0:55
    :END:
    - [X] Help GCI participants                  (4h)
** sandeepk
*** DONE Fluent Python Part II [1/1]
    CLOSED: <2019-12-11 Wed 21:55>
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   2.00
    :OWNER: sandeepk
    :ID: READ.1573385682
    :TASKID: READ.1573385682
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-11 Wed 20:55]--[2019-12-11 Wed 21:55] =>  1:00
    CLOCK: [2019-12-10 Tue 20:30]--[2019-12-10 Tue 21:00] =>  0:30
    CLOCK: [2019-12-04 Wed 20:05]--[2019-12-04 Wed 20:35] =>  0:30
    :END:
    - [X] Chapter-6  Design Patterns with First-Class Functions Part I (2h)
*** DONE Data Structure Part II [3/3]
    CLOSED: [2019-12-10 Tue 01:45]
    :PROPERTIES:
    :ESTIMATED: 9
    :ACTUAL:   5.05
    :OWNER: sandeepk
    :ID: READ.1573385745
    :TASKID: READ.1573385745
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-09 Mon 23:45]--[2019-12-10 Tue 01:45] =>  2:00
    CLOCK: [2019-12-09 Mon 00:23]--[2019-12-09 Mon 00:40] =>  0:17
    CLOCK: [2019-12-08 Sun 19:22]--[2019-12-08 Sun 20:00] =>  0:38
    CLOCK: [2019-12-06 Fri 22:47]--[2019-12-06 Fri 23:10] =>  0:23
    CLOCK: [2019-12-06 Fri 22:21]--[2019-12-06 Fri 22:40] =>  0:19
    CLOCK: [2019-12-05 Thu 20:19]--[2019-12-05 Thu 20:50] =>  0:31
    CLOCK: [2019-12-03 Tue 09:30]--[2019-12-03 Tue 10:05] =>  0:35
    CLOCK: [2019-12-02 Mon 09:50]--[2019-12-02 Mon 10:10] =>  0:20
    :END:
    - [X] Linked List        (2h)
    - [X] Stacks and Queues  (3h)
    - [X] Backtracking       (4h)
*** DONE Project Billion [3/3]
    CLOSED: [2019-12-04 Wed 01:15]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   3.00
    :OWNER: sandeepk
    :ID: OPS.1574962798
    :TASKID: OPS.1574962798
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-04 Wed 00:05]--[2019-12-04 Wed 01:15] =>  1:10
    CLOCK: [2019-11-30 Sat 18:00]--[2019-11-30 Sat 19:00] =>  1:00
    CLOCK: [2019-11-29 Fri 20:50]--[2019-11-29 Fri 21:10] =>  0:20
    CLOCK: [2019-11-28 Thu 20:00]--[2019-11-28 Thu 20:30] =>  0:30
    :END:
    - [X] GCP Instance Creation, App deployment and Database Setup.  (1h)
    - [X] Remote Connection Setup for Database.                      (30m)
    - [X] Cron job setup for Database update and event notification. (30m)
      

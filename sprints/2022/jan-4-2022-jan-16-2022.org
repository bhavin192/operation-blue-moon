#+TITLE: January 4-16, 2022 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2022-01-04 Tue>
  :wpd-akshay196: 1
  :wpd-nihaal: 1
  :wpd-bhavin192: 1
  :END:
** akshay196
*** DONE Read Kubernetes docs - Part VIII
    CLOSED: [2022-01-09 Sun 20:29]
    :PROPERTIES:
    :ESTIMATED: 4.5
    :ACTUAL:   3.05
    :OWNER:    akshay196
    :ID:       READ.1623432379
    :TASKID:   READ.1623432379
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-09 Sun 19:20]--[2022-01-09 Sun 20:29] =>  1:09
    CLOCK: [2022-01-08 Sat 22:14]--[2022-01-08 Sat 22:30] =>  0:16
    CLOCK: [2022-01-06 Thu 22:17]--[2022-01-06 Thu 22:34] =>  0:17
    CLOCK: [2022-01-04 Tue 20:46]--[2022-01-04 Tue 21:15] =>  0:29
    CLOCK: [2022-01-04 Tue 11:11]--[2022-01-04 Tue 12:03] =>  0:52
    :END:
    https://kubernetes.io/docs
   - [X] Secrets                                             (120m)
*** DONE Let's Go Book - Part VII
    CLOSED: [2022-01-15 Sat 17:41]
    :PROPERTIES:
    :ESTIMATED:  5
    :ACTUAL:   3.05
    :OWNER:    akshay196
    :ID:       READ.1629798238
    :TASKID:   READ.1629798238
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-15 Sat 17:15]--[2022-01-15 Sat 17:41] =>  0:26
    CLOCK: [2022-01-13 Thu 22:00]--[2022-01-13 Thu 22:53] =>  0:53
    CLOCK: [2022-01-12 Wed 19:30]--[2022-01-12 Wed 21:14] =>  1:44
    :END:
    - [X] 10.1. Generating a Self-Signed TLS Certificate    (1h)
    - [X] 10.2. Running a HTTPS Server                      (1h)
    - [X] 10.3. Configuring HTTPS Settings                  (1h)
    - [X] 10.4. Connection Timeouts                         (1h)
    - [X] 11.1. Routes Setup                                (30m)
    - [X] 11.2. Creating a Users Model                      (30m)
*** DONE Programming Kubernetes - Part II
    CLOSED: [2022-01-16 Sun 15:38]
    :PROPERTIES:
    :ESTIMATED: 3.5
    :ACTUAL:   1.83
    :OWNER: akshay196
    :ID: READ.1637477677
    :TASKID: READ.1637477677
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-16 Sun 14:51]--[2022-01-16 Sun 15:38] =>  0:47
    CLOCK: [2022-01-16 Sun 09:55]--[2022-01-16 Sun 10:42] =>  0:47
    CLOCK: [2022-01-15 Sat 19:55]--[2022-01-15 Sat 20:11] =>  0:16
    :END:
    - [X] Chapter 2. Kubernetes API Basics             (3.5h)
** bhavin192
*** DONE Watch KubeCon EU 2021 - Part III [9/9]
    CLOSED: [2022-01-09 Sun 20:20]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   3.18
    :OWNER:    bhavin192
    :ID:       READ.1629651751
    :TASKID:   READ.1629651751
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-09 Sun 20:08]--[2022-01-09 Sun 20:20] =>  0:12
    CLOCK: [2022-01-09 Sun 18:22]--[2022-01-09 Sun 18:43] =>  0:21
    CLOCK: [2022-01-09 Sun 16:45]--[2022-01-09 Sun 17:55] =>  1:10
    CLOCK: [2022-01-08 Sat 22:57]--[2022-01-08 Sat 23:23] =>  0:26
    CLOCK: [2022-01-05 Wed 22:25]--[2022-01-05 Wed 22:34] =>  0:09
    CLOCK: [2022-01-05 Wed 20:40]--[2022-01-05 Wed 21:05] =>  0:25
    CLOCK: [2022-01-04 Tue 22:17]--[2022-01-04 Tue 22:32] =>  0:15
    CLOCK: [2022-01-04 Tue 20:42]--[2022-01-04 Tue 20:55] =>  0:13
    :END:
    - [X] [[https://www.youtube.com/watch?v=H5eZEq_wqSE][Application Autoscaling Made Easy With Kubernetes Event-Driven Autoscaling - Tom Kerkhove, Codit - YouTube]]            (30m)
    - [X] [[https://www.youtube.com/watch?v=LedW9szFP1Y][Cloud Provider AWS Update and Roadmap - Nick Turner, Ayberk Yilmaz, Yang Yang, Amazon & Nicole Han - YouTube]]          (25m)
    - [X] [[https://www.youtube.com/watch?v=Yjr9s-aLHjU][Sponsored Session: Kubernetes Package Management Using Unix with Carvel, Helen George & João Pereira - YouTube]]        (25m)
    - [X] [[https://www.youtube.com/watch?v=pLInEuPnWv4][Panel Discussion: Cloud Native Networking State of the Union - YouTube]]                                                (35m)
    - [X] [[https://www.youtube.com/watch?v=LJJoaGszBVk][Sponsored Lightning Talk: Beyond Federation: Automating Multi-cloud Workloads... Kevin (Zefeng) Wang - YouTube]]        (05m)
    - [X] [[https://www.youtube.com/watch?v=jaTRWxrcUl8][High Throughput with Low Resource Usage: A Logging Journey - Eduardo Silva, Calyptia - YouTube]]                        (35m)
    - [X] [[https://www.youtube.com/watch?v=Mf7Y_u3QQ5I][Sponsored Session: HAProxy Technologies - Benchmarking 5 Popular Ingress Controllers - YouTube]]                        (10m)
    - [X] [[https://www.youtube.com/watch?v=vs8YrjdRJJU][Sponsored Session: Google Cloud - Multi-cluster, Blue-green Traffic Splitting with the Gateway API - YouTube]]          (15m)
    - [X] [[https://www.youtube.com/watch?v=0-f780xlsz0][Sponsored Session: Bridgecrew - End-to-end policy-as-code from IaC to Running Workloads - YouTube]]                     (15m)
*** DONE Write blog post about EmacsConf 2021
    CLOSED: [2022-01-16 Sun 22:59]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   4.70
    :OWNER: bhavin192
    :ID: WRITE.1642354650
    :TASKID: WRITE.1642354650
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-16 Sun 22:35]--[2022-01-16 Sun 22:59] =>  0:24
    CLOCK: [2022-01-16 Sun 19:48]--[2022-01-16 Sun 20:45] =>  0:57
    CLOCK: [2022-01-15 Sat 20:54]--[2022-01-15 Sat 21:33] =>  0:39
    CLOCK: [2022-01-14 Fri 23:40]--[2022-01-14 Fri 23:46] =>  0:06
    CLOCK: [2022-01-13 Thu 23:54]--[2022-01-14 Fri 00:03] =>  0:09
    CLOCK: [2022-01-12 Wed 21:53]--[2022-01-12 Wed 22:07] =>  0:14
    CLOCK: [2022-01-12 Wed 20:46]--[2022-01-12 Wed 21:00] =>  0:14
    CLOCK: [2022-01-11 Tue 20:34]--[2022-01-11 Tue 21:32] =>  0:58
    CLOCK: [2022-01-10 Mon 20:15]--[2022-01-10 Mon 21:16] =>  1:01
    :END:
*** DONE Work on LSW3 Prometheus exporter - Part I
    CLOSED: [2022-01-16 Sun 14:19]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   4.90
    :OWNER:    bhavin192
    :ID:       DEV.1642356784
    :TASKID:   DEV.1642356784
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-16 Sun 13:55]--[2022-01-16 Sun 14:19] =>  0:24
    CLOCK: [2022-01-15 Sat 13:01]--[2022-01-15 Sat 13:54] =>  0:53
    CLOCK: [2022-01-14 Fri 22:26]--[2022-01-14 Fri 23:10] =>  0:44
    CLOCK: [2022-01-14 Fri 15:07]--[2022-01-14 Fri 16:01] =>  0:54
    CLOCK: [2022-01-09 Sun 18:15]--[2022-01-09 Sun 18:21] =>  0:06
    CLOCK: [2022-01-08 Sat 14:38]--[2022-01-08 Sat 15:31] =>  0:53
    CLOCK: [2022-01-07 Fri 23:17]--[2022-01-08 Sat 00:17] =>  1:00
    :END:
** nihaal
*** DONE Read Linux Device Drivers, 3rd edition - Part V
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   4.82
    :OWNER: nihaal
    :ID: READ.1632069861
    :TASKID: READ.1632069861
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-16 Sun 18:32]--[2022-01-16 Sun 19:31] =>  0:59
    CLOCK: [2022-01-15 Sat 18:59]--[2022-01-15 Sat 20:56] =>  1:57
    CLOCK: [2022-01-11 Tue 19:18]--[2022-01-11 Tue 20:02] =>  0:44
    CLOCK: [2022-01-10 Mon 18:57]--[2022-01-10 Mon 20:06] =>  1:09
    :END:
    - [X] 7. Time, Delays, and Deferred Work          (2h)
    - [X] 8. Allocating Memory                        (3h)
*** DONE Read Linux Weekly News
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   2.28
    :OWNER: nihaal
    :ID: READ.1641269717
    :TASKID: READ.1641269717
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-13 Thu 18:57]--[2022-01-13 Thu 20:00] =>  1:03
    CLOCK: [2022-01-09 Sun 19:45]--[2022-01-09 Sun 20:16] =>  0:31
    CLOCK: [2022-01-09 Sun 18:44]--[2022-01-09 Sun 19:27] =>  0:43
    :END:
    - [X] Weekly edition for December 23, 2021        (1h)
    - [X] Weekly edition for January 6, 2021          (1h)
*** DONE Watch [[https://www.youtube.com/watch?v=16wUxqDf1GA][Coccinelle]] video
    :PROPERTIES:
    :ESTIMATED: 1.5
    :ACTUAL:   0.87
    :OWNER: nihaal
    :ID: READ.1641274133
    :TASKID: READ.1641274133
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-04 Tue 20:31]--[2022-01-04 Tue 20:49] =>  0:18
    CLOCK: [2022-01-04 Tue 19:25]--[2022-01-04 Tue 19:59] =>  0:34
    :END:
*** DONE Send revised version of [[https://lore.kernel.org/linux-staging/20211231172527.184788-1-abdun.nihaal@gmail.com/][this]] patch
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   4.17
    :OWNER: nihaal
    :ID: DEV.1641274351
    :TASKID: DEV.1641274351
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-06 Thu 21:52]--[2022-01-06 Thu 23:15] =>  1:23
    CLOCK: [2022-01-06 Thu 21:01]--[2022-01-06 Thu 21:25] =>  0:24
    CLOCK: [2022-01-05 Wed 19:40]--[2022-01-05 Wed 22:03] =>  2:23
    :END:

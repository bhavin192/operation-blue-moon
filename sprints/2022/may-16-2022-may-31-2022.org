#+TITLE: May 16-31, 2022 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2022-05-16 Mon>
  :wpd-bhavin192: 1
  :END:
** bhavin192
*** DONE Review FOSDEM 2022 videos
    CLOSED: [2022-05-30 Mon 22:38]
    :PROPERTIES:
    :ESTIMATED: 16
    :ACTUAL:   13.22
    :OWNER:    bhavin192
    :ID:       EVENT.1651401868
    :TASKID:   EVENT.1651401868
    :END:
    :LOGBOOK:
    CLOCK: [2022-05-30 Mon 22:15]--[2022-05-30 Mon 22:38] =>  0:23
    CLOCK: [2022-05-29 Sun 21:55]--[2022-05-29 Sun 22:34] =>  0:39
    CLOCK: [2022-05-27 Fri 23:36]--[2022-05-28 Sat 00:10] =>  0:34
    CLOCK: [2022-05-27 Fri 22:37]--[2022-05-27 Fri 23:18] =>  0:41
    CLOCK: [2022-05-26 Thu 22:45]--[2022-05-26 Thu 23:15] =>  0:30
    CLOCK: [2022-05-25 Wed 20:43]--[2022-05-25 Wed 21:04] =>  0:21
    CLOCK: [2022-05-24 Tue 21:50]--[2022-05-24 Tue 22:09] =>  0:19
    CLOCK: [2022-05-23 Mon 22:22]--[2022-05-23 Mon 22:49] =>  0:27
    CLOCK: [2022-05-23 Mon 21:05]--[2022-05-23 Mon 21:29] =>  0:24
    CLOCK: [2022-05-22 Sun 21:24]--[2022-05-22 Sun 21:40] =>  0:16
    CLOCK: [2022-05-22 Sun 20:04]--[2022-05-22 Sun 21:07] =>  1:03
    CLOCK: [2022-05-21 Sat 15:27]--[2022-05-21 Sat 16:51] =>  1:24
    CLOCK: [2022-05-21 Sat 12:17]--[2022-05-21 Sat 13:21] =>  1:04
    CLOCK: [2022-05-20 Fri 22:00]--[2022-05-20 Fri 22:52] =>  0:52
    CLOCK: [2022-05-19 Thu 22:55]--[2022-05-19 Thu 22:59] =>  0:04
    CLOCK: [2022-05-19 Thu 22:04]--[2022-05-19 Thu 22:45] =>  0:41
    CLOCK: [2022-05-19 Thu 20:54]--[2022-05-19 Thu 21:11] =>  0:17
    CLOCK: [2022-05-19 Thu 19:23]--[2022-05-19 Thu 20:50] =>  1:27
    CLOCK: [2022-05-19 Thu 18:47]--[2022-05-19 Thu 19:09] =>  0:22
    CLOCK: [2022-05-19 Thu 16:07]--[2022-05-19 Thu 16:16] =>  0:09
    CLOCK: [2022-05-19 Thu 15:05]--[2022-05-19 Thu 15:37] =>  0:32
    CLOCK: [2022-05-18 Wed 20:16]--[2022-05-18 Wed 21:00] =>  0:44
    :END:
